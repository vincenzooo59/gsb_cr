﻿namespace GSB_CR
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cBx_numRapp = new System.Windows.Forms.ComboBox();
            this.btn_rechercher = new System.Windows.Forms.Button();
            this.cBx_nom2 = new System.Windows.Forms.ComboBox();
            this.txt_motif2 = new System.Windows.Forms.TextBox();
            this.txt_Bilan2 = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTime2 = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btn_creer = new System.Windows.Forms.Button();
            this.cBx_nom1 = new System.Windows.Forms.ComboBox();
            this.txt_motif1 = new System.Windows.Forms.TextBox();
            this.txt_Bilan1 = new System.Windows.Forms.RichTextBox();
            this.txt_numRap = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTime1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GestionCompteRendus = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(469, 456);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cBx_numRapp);
            this.tabPage2.Controls.Add(this.btn_rechercher);
            this.tabPage2.Controls.Add(this.cBx_nom2);
            this.tabPage2.Controls.Add(this.txt_motif2);
            this.tabPage2.Controls.Add(this.txt_Bilan2);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.dateTime2);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(461, 430);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Consulter un compte rendu";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // cBx_numRapp
            // 
            this.cBx_numRapp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBx_numRapp.FormattingEnabled = true;
            this.cBx_numRapp.Location = new System.Drawing.Point(110, 95);
            this.cBx_numRapp.Name = "cBx_numRapp";
            this.cBx_numRapp.Size = new System.Drawing.Size(100, 21);
            this.cBx_numRapp.TabIndex = 27;
            // 
            // btn_rechercher
            // 
            this.btn_rechercher.Location = new System.Drawing.Point(166, 401);
            this.btn_rechercher.Name = "btn_rechercher";
            this.btn_rechercher.Size = new System.Drawing.Size(137, 23);
            this.btn_rechercher.TabIndex = 3;
            this.btn_rechercher.Text = "Rechercher";
            this.btn_rechercher.UseVisualStyleBackColor = true;
            this.btn_rechercher.Click += new System.EventHandler(this.btn_rechercher_Click);
            // 
            // cBx_nom2
            // 
            this.cBx_nom2.Enabled = false;
            this.cBx_nom2.FormattingEnabled = true;
            this.cBx_nom2.Location = new System.Drawing.Point(296, 95);
            this.cBx_nom2.Name = "cBx_nom2";
            this.cBx_nom2.Size = new System.Drawing.Size(140, 21);
            this.cBx_nom2.TabIndex = 26;
            // 
            // txt_motif2
            // 
            this.txt_motif2.Enabled = false;
            this.txt_motif2.Location = new System.Drawing.Point(296, 124);
            this.txt_motif2.Name = "txt_motif2";
            this.txt_motif2.Size = new System.Drawing.Size(140, 20);
            this.txt_motif2.TabIndex = 25;
            // 
            // txt_Bilan2
            // 
            this.txt_Bilan2.Enabled = false;
            this.txt_Bilan2.Location = new System.Drawing.Point(12, 175);
            this.txt_Bilan2.Name = "txt_Bilan2";
            this.txt_Bilan2.Size = new System.Drawing.Size(444, 219);
            this.txt_Bilan2.TabIndex = 24;
            this.txt_Bilan2.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 159);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Bilan :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(254, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Motif :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(236, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Praticien :";
            // 
            // dateTime2
            // 
            this.dateTime2.CustomFormat = "dd/mm/yyyy";
            this.dateTime2.Enabled = false;
            this.dateTime2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTime2.Location = new System.Drawing.Point(110, 124);
            this.dateTime2.Name = "dateTime2";
            this.dateTime2.Size = new System.Drawing.Size(100, 20);
            this.dateTime2.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(68, 126);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Date :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 98);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "N° de Rapport :";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(450, 72);
            this.label6.TabIndex = 16;
            this.label6.Text = "Gestion des Comptes Rendus";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btn_creer);
            this.tabPage1.Controls.Add(this.cBx_nom1);
            this.tabPage1.Controls.Add(this.txt_motif1);
            this.tabPage1.Controls.Add(this.txt_Bilan1);
            this.tabPage1.Controls.Add(this.txt_numRap);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.dateTime1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.GestionCompteRendus);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(461, 430);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Saisir un compte rendu";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btn_creer
            // 
            this.btn_creer.Location = new System.Drawing.Point(166, 401);
            this.btn_creer.Name = "btn_creer";
            this.btn_creer.Size = new System.Drawing.Size(137, 23);
            this.btn_creer.TabIndex = 14;
            this.btn_creer.Text = "Créer";
            this.btn_creer.UseVisualStyleBackColor = true;
            // 
            // cBx_nom1
            // 
            this.cBx_nom1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBx_nom1.FormattingEnabled = true;
            this.cBx_nom1.Location = new System.Drawing.Point(296, 95);
            this.cBx_nom1.Name = "cBx_nom1";
            this.cBx_nom1.Size = new System.Drawing.Size(140, 21);
            this.cBx_nom1.TabIndex = 13;
            // 
            // txt_motif1
            // 
            this.txt_motif1.Location = new System.Drawing.Point(296, 124);
            this.txt_motif1.Name = "txt_motif1";
            this.txt_motif1.Size = new System.Drawing.Size(140, 20);
            this.txt_motif1.TabIndex = 11;
            // 
            // txt_Bilan1
            // 
            this.txt_Bilan1.Location = new System.Drawing.Point(12, 175);
            this.txt_Bilan1.Name = "txt_Bilan1";
            this.txt_Bilan1.Size = new System.Drawing.Size(444, 219);
            this.txt_Bilan1.TabIndex = 10;
            this.txt_Bilan1.Text = "";
            // 
            // txt_numRap
            // 
            this.txt_numRap.Enabled = false;
            this.txt_numRap.Location = new System.Drawing.Point(110, 95);
            this.txt_numRap.Name = "txt_numRap";
            this.txt_numRap.Size = new System.Drawing.Size(100, 20);
            this.txt_numRap.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 159);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Bilan :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(254, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Motif :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(236, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Praticien :";
            // 
            // dateTime1
            // 
            this.dateTime1.CustomFormat = "dd/mm/yyyy";
            this.dateTime1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTime1.Location = new System.Drawing.Point(110, 124);
            this.dateTime1.Name = "dateTime1";
            this.dateTime1.Size = new System.Drawing.Size(100, 20);
            this.dateTime1.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Date :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "N° de Rapport :";
            // 
            // GestionCompteRendus
            // 
            this.GestionCompteRendus.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.GestionCompteRendus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GestionCompteRendus.Location = new System.Drawing.Point(6, 3);
            this.GestionCompteRendus.Name = "GestionCompteRendus";
            this.GestionCompteRendus.Size = new System.Drawing.Size(450, 72);
            this.GestionCompteRendus.TabIndex = 0;
            this.GestionCompteRendus.Text = "Gestion des Comptes Rendus";
            this.GestionCompteRendus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(209)))), ((int)(((byte)(234)))));
            this.ClientSize = new System.Drawing.Size(493, 480);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestion des Comptes Rendus";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cBx_numRapp;
        private System.Windows.Forms.Button btn_rechercher;
        private System.Windows.Forms.ComboBox cBx_nom2;
        private System.Windows.Forms.TextBox txt_motif2;
        private System.Windows.Forms.RichTextBox txt_Bilan2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTime2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btn_creer;
        private System.Windows.Forms.ComboBox cBx_nom1;
        private System.Windows.Forms.TextBox txt_motif1;
        private System.Windows.Forms.RichTextBox txt_Bilan1;
        private System.Windows.Forms.TextBox txt_numRap;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTime1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label GestionCompteRendus;
    }
}