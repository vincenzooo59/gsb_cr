﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace GSB_CR
{
    public partial class Form4 : Form
    {
        private MySqlConnection connect;

        public Form4()
        {
            InitializeComponent();
            connect = new MySqlConnection("SERVER=localhost;PORT=3306;DATABASE=gsb_cr;UID=gsbcr;PWD=serge;SslMode=none");

            connect.Open();

            // On rempli les comboBox "Type praticien" dans les onglets -Ajout d'un praticien -Modifier un praticien
            MySqlDataReader readercbxTypePraticien;
            MySqlCommand cbx_TypePraticien = new MySqlCommand(("SELECT DISTINCT libelle FROM type_praticien"), connect);
            readercbxTypePraticien = cbx_TypePraticien.ExecuteReader();
            while (readercbxTypePraticien.Read())
            {
                cbxAjoutTypePraticien.Items.Add(readercbxTypePraticien.GetString("libelle"));
                cbxModifTypePraticien.Items.Add(readercbxTypePraticien.GetString("libelle"));
            }            
            cbx_TypePraticien.Parameters.Clear();
            readercbxTypePraticien.Close();

            //-------------------------------------------------------------------------------------------------------
            remplissageListePraticien();
            remplissageCbxNomPrenom();

        }

        public void remplissageCbxNomPrenom() //On remplit la comboBox Nom + " " + Prenom dans l'onglet -Modification un praticien
        {
            cbxNomPrenomModifPraticien.Items.Clear();
            MySqlDataReader readercbxModifPraticien;
            MySqlCommand reqNomPrenomModifPraticien = new MySqlCommand(("SELECT nom, prenom FROM praticien"), connect);
            readercbxModifPraticien = reqNomPrenomModifPraticien.ExecuteReader();
            while (readercbxModifPraticien.Read())
            {
                cbxNomPrenomModifPraticien.Items.Add(readercbxModifPraticien.GetString("prenom") + " " + readercbxModifPraticien.GetString("nom"));
                cbxSupprPraticien.Items.Add(readercbxModifPraticien.GetString("prenom") + " " + readercbxModifPraticien.GetString("nom"));
            }
            reqNomPrenomModifPraticien.Parameters.Clear();
            readercbxModifPraticien.Close();
        }

        public void remplissageListePraticien()
        {
            MySqlDataReader readerListePraticien;
            int numPraticien;
            string nomPraticen;
            string prenomPraticien;
            string adressePraticien;
            string ville;
            string typePraticien;
            MySqlCommand listePraticien = new MySqlCommand("SELECT praticien.id, praticien.nom, praticien.prenom, praticien.adresse, ville.code_postal, ville.nom AS nomVille, type_praticien.libelle FROM praticien, type_praticien, ville WHERE ville.id = praticien.id_Ville AND praticien.id_Type_Praticien = type_praticien.id ORDER BY id ASC", connect);
            readerListePraticien = listePraticien.ExecuteReader();
            while (readerListePraticien.Read())
            {
                numPraticien = readerListePraticien.GetInt32("id");
                nomPraticen = readerListePraticien.GetString("nom");
                prenomPraticien = readerListePraticien.GetString("prenom");
                adressePraticien = readerListePraticien.GetString("adresse");
                ville = readerListePraticien.GetString("code_postal") + " " + readerListePraticien.GetString("nomVille");
                typePraticien = readerListePraticien.GetString("libelle");
                dataGridView1.Rows.Add(numPraticien, nomPraticen, prenomPraticien, adressePraticien, ville, typePraticien);
            }
            listePraticien.Parameters.Clear();
            readerListePraticien.Close();
        }

        private void cbxModifPraticien_SelectedIndexChanged(object sender, EventArgs e)
        {
            // On active tous les champs
            txtNomModifPraticien.Enabled = true;
            txtPrenomModifPraticien.Enabled = true;
            txtAdresseModifPraticien.Enabled = true;
            txtCPModifPraticien.Enabled = true;
            txtVilleModifPraticien.Enabled = true;
            cbxModifTypePraticien.Enabled = true;

            // On instancie nos differentes variables

            int id_Ville = 0;
            int id_Type_Praticien = 0;
            int id = cbxNomPrenomModifPraticien.SelectedIndex + 1;

            // On remplie les champs nom, prenom, adresse | et on recupère nos clé etrangers id_Ville et id_Type_Praticien
            MySqlDataReader readerModifPraticien;
            MySqlCommand reqNomPrenomModif = new MySqlCommand("SELECT nom, prenom, adresse, id_Ville, id_Type_Praticien FROM praticien WHERE id = @id", connect);
            reqNomPrenomModif.Parameters.AddWithValue("@id", id);
            readerModifPraticien = reqNomPrenomModif.ExecuteReader();
            while (readerModifPraticien.Read())
            {
                txtNomModifPraticien.Text = readerModifPraticien.GetString("nom");
                txtPrenomModifPraticien.Text = readerModifPraticien.GetString("prenom");
                txtAdresseModifPraticien.Text = readerModifPraticien.GetString("adresse");
                id_Ville = readerModifPraticien.GetInt32("id_Ville");
                id_Type_Praticien = readerModifPraticien.GetInt32("id_Type_Praticien");
            }        
            reqNomPrenomModif.Parameters.Clear();
            readerModifPraticien.Close();

            // On remplie les champs Code Postal et Ville
            MySqlDataReader readerVillePraticien;
            MySqlCommand reqVillePraticien = new MySqlCommand("SELECT ville.nom, ville.code_postal FROM ville, praticien WHERE praticien.id_Ville = ville.id AND praticien.id = @id", connect);
            reqVillePraticien.Parameters.AddWithValue("@id", id);
            readerVillePraticien = reqVillePraticien.ExecuteReader();
            while (readerVillePraticien.Read())
            {                
                txtCPModifPraticien.Text = readerVillePraticien.GetInt32("code_postal").ToString();
                txtVilleModifPraticien.Text = readerVillePraticien.GetString("nom");
            }
            reqVillePraticien.Parameters.Clear();
            readerVillePraticien.Close();

            // On remplie notre comboBox : Type Praticien
            MySqlCommand reqTypeModifPraticien = new MySqlCommand("SELECT type_praticien.libelle FROM type_praticien, praticien WHERE praticien.id_Type_Praticien = type_praticien.id AND praticien.id = @id", connect);
            reqTypeModifPraticien.Parameters.AddWithValue("@id", id);
            reqTypeModifPraticien.ExecuteNonQuery();
            cbxModifTypePraticien.Text = reqTypeModifPraticien.ExecuteScalar().ToString();        

        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            try
            {
                int id = cbxNomPrenomModifPraticien.SelectedIndex + 1;
                int id_Type_Praticien = 0;

                MySqlCommand req1 = new MySqlCommand("SELECT praticien.id_Type_Praticien FROM type_praticien, praticien WHERE praticien.id_Type_Praticien = type_praticien.id AND type_praticien.libelle = @libelle", connect);
                req1.Parameters.AddWithValue("@libelle", cbxModifTypePraticien.Text);
                req1.ExecuteNonQuery();
                id_Type_Praticien = int.Parse(req1.ExecuteScalar().ToString());
                req1.Parameters.Clear();

                int nbVille = 0;
                MySqlCommand reqNbVille = new MySqlCommand("SELECT COUNT(*) FROM ville WHERE nom = @nomVille", connect);
                reqNbVille.Parameters.AddWithValue("@nomVille", txtVilleModifPraticien.Text.ToUpper());
                reqNbVille.ExecuteNonQuery();
                nbVille = int.Parse(reqNbVille.ExecuteScalar().ToString());
                reqNbVille.Parameters.Clear();

                int id_Ville;

                if (nbVille > 0)
                {
                    MySqlCommand reqIdNomVille = new MySqlCommand("SELECT id FROM ville WHERE nom = @nomVille", connect);
                    reqIdNomVille.Parameters.AddWithValue("@nomVille", txtVilleModifPraticien.Text.ToUpper());
                    reqIdNomVille.ExecuteNonQuery();
                    id_Ville = int.Parse(reqIdNomVille.ExecuteScalar().ToString());
                    reqIdNomVille.Parameters.Clear();
                }
                else
                {
                    MySqlCommand reqAjoutVille = new MySqlCommand("INSERT INTO ville(nom, code_postal) VALUES (@nomVille, @codePostal)", connect);
                    reqAjoutVille.Parameters.AddWithValue("@nomVille", txtVilleModifPraticien.Text.ToUpper());                    
                    reqAjoutVille.Parameters.AddWithValue("@codePostal", txtCPModifPraticien.Text);
                    reqAjoutVille.ExecuteNonQuery();
                    reqAjoutVille.Parameters.Clear();

                    MySqlCommand reqRecupIdVille = new MySqlCommand("SELECT id FROM ville WHERE nom = @nomVille", connect);
                    reqRecupIdVille.Parameters.AddWithValue("@nomVille", txtVilleModifPraticien.Text);
                    reqRecupIdVille.ExecuteNonQuery();
                    id_Ville = int.Parse(reqRecupIdVille.ExecuteScalar().ToString());
                    reqRecupIdVille.Parameters.Clear();
                }

                MySqlCommand req3 = new MySqlCommand("UPDATE praticien SET nom = @nom, prenom = @prenom, adresse = @adresse, id_Ville = @id_Ville, id_Type_Praticien = @id_Type_Praticien WHERE id = @id", connect);
                req3.Parameters.AddWithValue("@nom", txtNomModifPraticien.Text);
                req3.Parameters.AddWithValue("@prenom", txtPrenomModifPraticien.Text);
                req3.Parameters.AddWithValue("@adresse", txtAdresseModifPraticien.Text);
                req3.Parameters.AddWithValue("@id_Ville", id_Ville);
                req3.Parameters.AddWithValue("@id_Type_Praticien", id_Type_Praticien);
                req3.Parameters.AddWithValue("@id", id);
                req3.ExecuteNonQuery();
                req3.Parameters.Clear();

                txtNomModifPraticien.Enabled = false;
                txtPrenomModifPraticien.Enabled = false;
                txtAdresseModifPraticien.Enabled = false;
                txtCPModifPraticien.Enabled = false;
                txtVilleModifPraticien.Enabled = false;
                cbxModifTypePraticien.Enabled = false;

                cbxNomPrenomModifPraticien.Text = "";
                txtNomModifPraticien.Text = "";
                txtPrenomModifPraticien.Text = "";
                txtAdresseModifPraticien.Text = "";
                txtCPModifPraticien.Text = "";
                txtVilleModifPraticien.Text = "";
                cbxModifTypePraticien.Text = " ";

                remplissageListePraticien();
                remplissageCbxNomPrenom();

                MessageBox.Show("Le praticien a été modifié avec succés");
            }
            catch(NullReferenceException)
            {
                MessageBox.Show("Veuillez remplir le formulaire");
            }
        }

        private void Form4_FormClosed(object sender, FormClosedEventArgs e)
        {
            connect.Close();
            this.Close();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            MySqlDataReader readerTypePraticien;
            MySqlCommand typePraticien = new MySqlCommand(("SELECT DISTINCT libelle FROM type_praticien"), connect);
            readerTypePraticien = typePraticien.ExecuteReader();
            while (readerTypePraticien.Read())
            {
                cbxAjoutTypePraticien.Items.Add(readerTypePraticien.GetString("libelle"));
            }
            typePraticien.Parameters.Clear();
            readerTypePraticien.Close();

            int nbVille = 0;
            MySqlCommand reqNbVille = new MySqlCommand("SELECT COUNT(*) FROM ville WHERE nom = @nomVille", connect);
            reqNbVille.Parameters.AddWithValue("@nomVille", txtVille.Text);
            reqNbVille.ExecuteNonQuery();
            nbVille = int.Parse(reqNbVille.ExecuteScalar().ToString());
            reqNbVille.Parameters.Clear();

            int idVille;

            if (nbVille > 0)
            {                
                MySqlCommand req1 = new MySqlCommand("SELECT id FROM ville WHERE nom = @nomVille", connect);
                req1.Parameters.AddWithValue("@nomVille", txtVille.Text);
                req1.ExecuteNonQuery();
                idVille = int.Parse(req1.ExecuteScalar().ToString());
                req1.Parameters.Clear();
            }
            else
            {
                MySqlCommand reqAjoutVille = new MySqlCommand("INSERT INTO ville(nom, code_postal) VALUES (@nomVille, @codePostal)", connect);
                reqAjoutVille.Parameters.AddWithValue("@nomVille", txtVille.Text.ToUpper());
                reqAjoutVille.Parameters.AddWithValue("@codePostal", txtCodePostal.Text);
                reqAjoutVille.ExecuteNonQuery();
                reqAjoutVille.Parameters.Clear();

                MySqlCommand reqRecupIdVille = new MySqlCommand("SELECT id FROM ville WHERE nom = @nomVille", connect);
                reqRecupIdVille.Parameters.AddWithValue("@nomVille", txtVille.Text);
                reqRecupIdVille.ExecuteNonQuery();
                idVille = int.Parse(reqRecupIdVille.ExecuteScalar().ToString());
                reqRecupIdVille.Parameters.Clear();
            }

            int id_Type_Praticien;
            MySqlCommand req2 = new MySqlCommand("SELECT id FROM type_praticien WHERE libelle = @typePraticien", connect);
            req2.Parameters.AddWithValue("@typePraticien", cbxAjoutTypePraticien.Text);
            req2.ExecuteNonQuery();
            id_Type_Praticien = int.Parse(req2.ExecuteScalar().ToString());
            req2.Parameters.Clear();

            MySqlCommand req = new MySqlCommand("INSERT INTO praticien(nom, prenom, adresse, id_Ville, id_Type_Praticien) VALUES(@nom, @prenom, @adresse, @id_Ville, @id_Type_Praticien)", connect);
            req.Parameters.AddWithValue("@nom", txtNom.Text);
            req.Parameters.AddWithValue("@prenom", txtPrenom.Text);
            req.Parameters.AddWithValue("@adresse", txtAdresse.Text);
            req.Parameters.AddWithValue("@id_Ville", idVille);
            req.Parameters.AddWithValue("@id_Type_Praticien", id_Type_Praticien);

            req.ExecuteNonQuery();
            req.Parameters.Clear();

            remplissageListePraticien();
            remplissageCbxNomPrenom();

            MessageBox.Show("Le praticien a été ajouté avec succés");

            /*else
            {
                MessageBox.Show("Veuillez renseigner tout les champs");
            }*/
        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            MySqlCommand reqSupprPraticien = new MySqlCommand("DELETE FROM praticien WHERE id = @id", connect);
            reqSupprPraticien.Parameters.AddWithValue("@id", cbxSupprPraticien.SelectedIndex + 1);
            reqSupprPraticien.ExecuteNonQuery();
            reqSupprPraticien.Parameters.Clear();

            remplissageCbxNomPrenom();
            remplissageListePraticien();

            cbxSupprPraticien.SelectedIndex = -1;
        }
    }
}
